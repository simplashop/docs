var gulp = require('gulp'),
    autoprefixer = require('gulp-autoprefixer'),
    sass = require('gulp-sass'),
    sourcemaps = require('gulp-sourcemaps'),
    bs	 = require('browser-sync'),
    path = 'design/template-name/'; //обязатально указать свой путь

/*задача для эмуляции сервера*/
gulp.task('bs', function(){
  bs({
    proxy: 'site.local', //указать локальный адрес на своем сервере
    port: 433,
    notify: false
  });
});

/*задача для компиляции css*/
gulp.task('sass', function(){
  return gulp.src(path+'sass/*.scss')
    .pipe(sourcemaps.init()) // нициализируем sourcemaps
    .pipe(sass({
      outputStyle: 'compressed' // минифицируем компилируемый css
    }).on('error', sass.logError))
    .pipe(autoprefixer({
      browsers: ['> 5%', 'last 2 versions', 'ie >= 10'] // добавляем вендорные префиксы
    }))
    .pipe(sourcemaps.write('.')) //сохраняем sourcemap файлы в ту же директорию, что и css
    .pipe(gulp.dest(path+'css'))
    .pipe(bs.reload({stream: true}));
});

/*Задача на отслеживание изменений*/
gulp.task('watch', ['sass', 'bs'], function(){
  gulp.watch(path+'sass/*.scss', ['sass']);
  gulp.watch(path+'js/**/*.js').on('change', bs.reload);
  gulp.watch(path+'html/*.tpl').on('change', bs.reload);
  gulp.watch('compiled/**/*.php').on('change', bs.reload);
});

/*Назначаем задачу по умолчанию*/
gulp.task('default', ['bs', 'sass', 'watch']);